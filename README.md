# protoc-gen-doc

Copy of [protoc-gen-doc] Docker image, extended with Git. The default entrypoint
is removed, so Git can be used e.g. in a CI script like this:

    protobuf:
      stage: build
      image:
        name: registry.gitlab.vgiscience.org/tools/protoc-gen-doc
      script:
        - git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.vgiscience.de/lbsn/structure/protobuf.git/
        - cp resources/proto-markdown-tmpl/markdown_lbsn.tmpl protos/
        - cp protobuf/lbsnstructure/* protos/
        - bash /entrypoint.sh --doc_out=out/ --doc_opt=protos/markdown_lbsn.tmpl,protobuf.md social.proto topical.proto spatial.proto temporal.proto interlinkage.proto
      artifacts:
        paths:
          - out/protobuf.md

[protoc-gen-doc]: https://github.com/pseudomuto/protoc-gen-doc
