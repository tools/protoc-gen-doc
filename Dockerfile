FROM pseudomuto/protoc-gen-doc

RUN apk --update add ca-certificates git --no-cache \
    && rm -rf /var/cache/apk/*
